﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YemekSepeti.Assessment.Case
{
    class Program
    {
        static void Main(string[] args)
        {
            //Case 1 for Question 2
            int[] arr1 = { 1, 5, 2, 8 };
            //Case 2 for Question 1
            int[] arr2 = { -7, 1, 5, 2, -4, 3, 0 };

            int arr2_size = arr2.Length;
            int arr1_size = arr1.Length;
            Console.WriteLine(Equilibrium(arr1, arr1_size));
            Console.ReadLine();
            
            /*
            Tuple<int, int> indices = FindTwoSum(new List<int>() { 1, 3, 5, 7, 9 }, 12);
            Console.WriteLine(indices.Item1 + " " + indices.Item2);
            Console.ReadLine();
            */
        }
        /// <summary>
        /// Question 1
        /// </summary>
        /// <param name="arr"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        static int Equilibrium(int[] arr, int n)
        {
            int i, j;
            int leftsum, rightsum;

            /* Check for indexes one by one until an equilibrium
               index is found */
            for (i = 0; i < n; ++i)
            {
                leftsum = 0;  // initialize left sum for current index i
                rightsum = 0; // initialize right sum for current index i

                /* get left sum */
                for (j = 0; j < i; j++)
                    leftsum += arr[j];

                /* get right sum */
                for (j = i + 1; j < n; j++)
                    rightsum += arr[j];

                /* if leftsum and rightsum are same, then we are done */
                if (leftsum == rightsum)
                    return i;
            }

            /* return -1 if no equilibrium index is found */
            return -1;
        }
        /// <summary>
        /// Find Two Sum in array 
        /// </summary>
        /// <param name="list"></param>
        /// <param name="sum"></param>
        /// <returns></returns>
        static Tuple<int, int> FindTwoSum(IList<int> list, int sum)
        {
            for (int i = 0; i < list.Count; i++)
            {
                // subtract the item to the sum to find the difference
                int diff = sum - list[i];

                // if that number exist in that list find its index
                if (list.Contains(diff))
                {
                    return Tuple.Create(i, list.IndexOf(diff));
                }
            }

            return null;
        }
    }
}
